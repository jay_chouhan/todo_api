const mysql = require('mysql2');
const { host, user, password, database, connectionLimit } = require('./config');

const pool = mysql.createPool({
  host,
  user,
  password,
  database,
  connectionLimit,
});

pool.getConnection((err, conn) => {
  if (err) {
    console.error(err);
  } else {
    console.log('database connected');
  }
});

module.exports = pool;

const jwt = require('jsonwebtoken');

module.exports = (data, secret, options) => {
  return new Promise((resolve, reject) => {
    jwt.sign(data, secret, options, (error, token) => {
      if (error) {
        return reject(error);
      }

      resolve(token);
    });
  });
};

require('dotenv').config();
const db = require('../db');
const jwt = require('jsonwebtoken');
const generateToken = require('./generateToken');

const loginUser = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { username, id, password } = user[0];
      const accessTokenPromise = generateToken({ username, id }, process.env.ACCESS_TOKEN_SECRET, {
        expiresIn: '300s',
      });
      const refreshTokenPromise = generateToken({ username, id }, process.env.REFRESH_TOKEN_SECRET + password);

      const [accessToken, refreshToken] = await Promise.all([accessTokenPromise, refreshTokenPromise]);

      const rows = await db.promise().query(`select * from refresh_tokens where user_id = ?`, [id]);
      if (rows[0].length === 0) {
        db.query(`insert into refresh_tokens (user_id,token) values (?,?)`, [id, refreshToken]);
      } else {
        db.query(`update refresh_tokens set token = "?" where user_id = ?`, [refreshToken, id]);
      }

      resolve({ accessToken, refreshToken });
    } catch (err) {
      reject(err);
    }
  });
};

module.exports = loginUser;

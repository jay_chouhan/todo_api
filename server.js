require('dotenv').config();

const express = require('express');
const logger = require('./middleware/logger');
const usersRouter = require('./routes/auth');
const tasksRouter = require('./routes/tasks');
const subtasksRouter = require('./routes/subtasks');
const { port } = require('./config');

const app = express();

const PORT = port;

app.use(logger);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/users', usersRouter);
app.use('/tasks', tasksRouter);
app.use('/subtasks', subtasksRouter);

app.use('/', (req, res) => res.status(404).json({ msg: 'Not found' }));

app.listen(PORT, () => console.log(`server is starting on port ${PORT}`));

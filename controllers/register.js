const bcrypt = require('bcrypt');
const db = require('../db');
const login = require('../modules/login');

module.exports = async (req, res) => {
  try {
    const { username, password } = req.body;
    const hash = await bcrypt.hash(password, 10);

    const insertQuery = await db
      .promise()
      .query(`insert into users (username,password) values (?,?)`, [username, hash]);

    const rows = await db.promise().query(`select * from users where username = ?`, [username]);

    const user = rows[0];

    const { accessToken, refreshToken } = await login(user);

    return res.status(201).json({ accessToken, refreshToken });
  } catch (err) {
    console.error(err);
    return res.status(500).json({ msg: `something's wrong` });
  }
};

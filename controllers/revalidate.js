const jwt = require('jsonwebtoken');
const generateToken = require('../modules/generateToken');
const db = require('../db');

module.exports = async (req, res) => {
  try {
    const refreshHeader = req.headers['x-refresh-token'];
    const refreshToken = refreshHeader.split(' ')[1];
    const { username, id } = jwt.decode(refreshToken);

    const refreshQuery = await db.promise().query(`select * from refresh_tokens where user_id = ?`, [id]);
    const refreshData = refreshQuery[0];

    if (refreshData[0] === undefined) {
      return res.status(401).json({ msg: `you're not logged in` });
    }

    if (refreshData[0].token === refreshToken) {
      const rows = await db.promise().query(`select * from users where id = ?`, id);
      const user = rows[0];
      const { password } = user[0];
      jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET + password, async (error, decoded) => {
        if (error) {
          console.log(error);
        } else {
          const accessToken = await generateToken({ username, id }, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: '300s',
          });
          res.json({ accessToken });
        }
      });
    } else {
      throw new Error('Invalid refresh token');
    }
  } catch (error) {
    console.error(error);
    res.status(400).json({ msg: 'Revalidation Required' });
  }
};

const jwt = require('jsonwebtoken');
const db = require('../db');

module.exports = async (req, res) => {
  try {
    const refreshHeader = req.headers['x-refresh-token'];
    const refreshToken = refreshHeader.split(' ')[1];
    const { id } = jwt.decode(refreshToken);

    const rowCount = await db.promise().query(`delete from refresh_tokens where token = ?`, [refreshToken]);
    if (rowCount[0]) {
      res.json({ msg: 'Logout Success' });
    } else {
      throw new Error('No Refresh Token in DB');
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({ msg: 'Invalid Token' });
  }
};

const bcrypt = require('bcrypt');
const db = require('../db');
const login = require('../modules/login');

module.exports = async (req, res) => {
  try {
    const { username, password } = req.body;
    const rows = await db.promise().query(`select * from users where username = ?`, [username]);

    if (rows[0] === undefined) {
      return res.status(401).json({ msg: 'No user found' });
    }

    const user = rows[0];
    const result = await bcrypt.compare(password, user[0].password);

    if (!result) {
      return res.status(401).json({ msg: 'Password incorrect' });
    }

    const { accessToken, refreshToken } = await login(user);

    res.json({ message: 'login success', accessToken, refreshToken });
  } catch (error) {
    console.error(error);
  }
};

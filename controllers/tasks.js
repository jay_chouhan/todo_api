const db = require('../db');

const getAllTasks = (req, res) => {
  db.query('SELECT * FROM tasks', (err, rows, fields) => {
    if (err) {
      res.status(500).send({ error: 'something wrong' });
    } else {
      res.status(200).json(rows);
    }
  });
};

const getTaskById = (req, res) => {
  const id = req.params.id;

  db.query(`SELECT * FROM tasks WHERE id=?`, [id], (err, row, fields) => {
    if (err) {
      res.status(500).send({ error: 'Something failed!' });
    }
    if (row[0]) {
      res.status(200).json(row[0]);
    } else {
      res.status(404).json({ msg: 'not found' });
    }
  });
};

const newTask = (req, res) => {
  const user_id = req.user.id;
  const task = req.body.task;

  db.query(`INSERT INTO tasks(user_id,task,task_status) VALUES (?,?,"0")`, [user_id, task], (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).send({ error: 'Something failed!' });
    }
    res.status(201).json({ status: 'success' });
  });
};

const updateTask = async (req, res, next) => {
  const id = req.params.id;
  const status = req.body.status;

  const [rows] = await db.promise().query(`select * from tasks where id = ?`, [id]);

  if (rows.length === 0) {
    return res.status(404).json({ msg: 'not found' });
  }
  const { user_id } = rows[0];

  if (user_id === req.user.id) {
    db.query(`UPDATE tasks SET task_status=? WHERE id=?`, [status, id], (err, result) => {
      if (err) {
        return res.status(500).send({ error: 'Something failed!' });
      }
      res.status(201).json({
        status: 'completed',
      });
    });
  } else {
    res.status(401).json({ msg: 'not authorised' });
  }
};

const deleteTask = async (req, res) => {
  const id = req.params.id;

  const [rows] = await db.promise().query(`select * from tasks where id = ?`, [id]);

  if (rows.length === 0) {
    return res.status(404).json({ msg: 'not found' });
  }
  const { user_id } = rows[0];
  if (user_id === req.user.id) {
    db.query(`DELETE FROM tasks WHERE id=?`, [id], (err, result) => {
      if (err) {
        res.status(500).send({ error: 'Something failed!' });
      }
      res.json({ status: 'success' });
    });
  } else {
    res.status(401).json({ msg: 'not authorised' });
  }
};

module.exports = { getAllTasks, getTaskById, newTask, updateTask, deleteTask };

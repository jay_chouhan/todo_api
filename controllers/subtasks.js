const db = require('../db');

const getAllSubtasks = (req, res) => {
  db.query('SELECT * FROM subtasks', (err, rows, fields) => {
    if (err) {
      res.status(500).send({ error: 'something wrong' });
    } else {
      res.status(200).json(rows);
    }
  });
};

const getSubtaskById = (req, res) => {
  const id = req.params.id;

  db.query(`SELECT * FROM subtasks WHERE id=?`, [id], (err, row, fields) => {
    if (err) {
      res.status(500).send({ error: 'Something failed!' });
    }
    if (row[0]) {
      res.status(200).json(row[0]);
    } else {
      res.status(404).json({ msg: 'not found' });
    }
  });
};

const newSubtask = async (req, res) => {
  const user_id = req.user.id;
  const task_id = req.body.taskId;
  const subtask = req.body.subtask;

  const rows = await db.promise().query(`select * from tasks where id = ? and user_id = ?`, [task_id, user_id]);
  if (rows[0].length === 0) {
    return res.status(401).json({ msg: 'not authorised' });
  }

  db.query(
    `INSERT INTO subtasks(task_id,subtask,subtask_status) VALUES (?,?,"0")`,
    [task_id, user_id, subtask],
    (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).send({ error: 'Something failed!' });
      }
      res.status(201).json({ status: 'success' });
    }
  );
};

const updateSubtask = async (req, res, next) => {
  const id = req.params.id;
  const status = req.body.status;

  const [rows] = await db.promise().query(`select * from subtasks where id = ?`, [id]);

  if (rows.length === 0) {
    return res.status(404).json({ msg: 'not found' });
  }
  const { task_id } = rows[0];

  const [user] = await db.promise().query(`select user_id from tasks where id = ?`, [task_id]);
  const { user_id } = user[0];

  if (user_id === req.user.id) {
    db.query(`UPDATE subtasks SET subtask_status=? WHERE id=?`, [status, id], (err, result) => {
      if (err) {
        return res.status(500).send({ error: 'Something failed!' });
      }
      res.status(201).json({
        status: 'completed',
      });
    });
  } else {
    res.status(401).json({ msg: 'not authorised' });
  }
};

const deleteSubtask = async (req, res) => {
  const id = req.params.id;

  const [rows] = await db.promise().query(`select * from subtasks where id = ?`, [id]);

  if (rows.length === 0) {
    return res.status(404).json({ msg: 'not found' });
  }
  const { task_id } = rows[0];

  const [user] = await db.promise().query(`select user_id from tasks where id = ?`, [task_id]);
  const { user_id } = user[0];

  if (user_id === req.user.id) {
    db.query(`DELETE FROM subtasks WHERE id=?`, [id], (err, result) => {
      if (err) {
        res.status(500).send({ error: 'Something failed!' });
      }
      res.json({ status: 'success' });
    });
  } else {
    res.status(401).json({ msg: 'not authorised' });
  }
};

module.exports = { getAllSubtasks, getSubtaskById, newSubtask, updateSubtask, deleteSubtask };

const express = require('express');
const { getAllTasks, getTaskById, newTask, updateTask, deleteTask } = require('../controllers/tasks');
const db = require('../db');
const authorization = require('../middleware/authorization');
const router = express.Router();

router.use(express.json());

router.get('/', authorization, getAllTasks);

router.get('/:id', authorization, getTaskById);

router.post('/', authorization, newTask);

router.patch('/:id', authorization, updateTask);

router.delete('/:id', authorization, deleteTask);

module.exports = router;
